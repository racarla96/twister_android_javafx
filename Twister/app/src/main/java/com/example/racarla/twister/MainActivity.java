package com.example.racarla.twister;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private LinearLayout mainLayout;
    private Toolbar mainBar;

    private Button random;
    private TextToSpeech tts;

    private boolean started = false;

    private int time = 25;
    private int players = 3;

    private SeekBar playerSpaceTime;
    private SeekBar numberPlayers;

    private TextView showNumberPlayers;
    private TextView showTimeSpacePlayers;

    private String mascara = "Numero de jugadores: ";
    private Timer timer;
    private TimerTask timerTask;
    final Handler handler = new Handler();

    private ArrayList<Integer> playersOrder = new ArrayList<>();
    private int counter = 0;

    private String[] names = {"Jugador Uno","Jugador Dos", "Jugador Tres", "Jugador Cuatro", "Jugador Cinco", "Jugador Seis"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainBar = (Toolbar) findViewById(R.id.mainBar);
        setSupportActionBar(mainBar);

        playerSpaceTime = (SeekBar) findViewById(R.id.playerSpaceTime);
        numberPlayers = (SeekBar) findViewById(R.id.numPlayers);

        showTimeSpacePlayers = (TextView) findViewById(R.id.time);
        showNumberPlayers = (TextView) findViewById(R.id.players);

        playerSpaceTime.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        time = getTime(playerSpaceTime.getProgress());
                        showTimeSpacePlayers.setText(time + "");
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

        numberPlayers.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        players = getPlayers(numberPlayers.getProgress());
                        showNumberPlayers.setText(mascara + players);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

        mainLayout = (LinearLayout) findViewById(R.id.mainLayout);

        random = (Button) findViewById(R.id.Random);

        // Init speaking text
        tts=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
            }
        });
        Locale locSpanish = new Locale("spa", "ESP");
        tts.setLanguage(locSpanish);
        tts.setSpeechRate(0.8f);

        // Initial page
        ((ImageView) findViewById(R.id.selection)).setColorFilter(getResources().getColor(R.color.rojo));

        random.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(started) {
                    started = false;
                    playerSpaceTime.setEnabled(true);
                    numberPlayers.setEnabled(true);
                    random.setText(R.string.start);
                    stopTimerTask();
                } else {
                    started = true;
                    playerSpaceTime.setEnabled(false);
                    numberPlayers.setEnabled(false);
                    random.setText(R.string.stop);
                    startTimer();
                    counter = players; // Reset
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                if(!started) {
                    Intent intent = new Intent(this, players_name.class);
                    intent.putExtra("NUMPLAYERS", players);
                    intent.putExtra("NAMES", names);
                    startActivityForResult(intent, 101);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case 101:
                names = data.getStringArrayExtra("UPDATENAMES");
                //for(int i = 0; i < names.length; i++)  tts.speak(names[i], TextToSpeech.QUEUE_ADD, null, null);
                break;
        }
    }

    private void twist(){
        if(counter == players) {
            counter = 0;
            // Randomize Order
            playersOrder.clear();
            for(int i = 0; i < players; i++) playersOrder.add(i);
            Collections.shuffle(playersOrder);
        }
        ((ImageView) findViewById(R.id.selection)).setScaleX(1f);
        String txtColor = "en el color ";
        String text = "";
        // Pie o Mano
        boolean footOrHand = new Random().nextBoolean();
        if(footOrHand){
            ((ImageView) findViewById(R.id.selection)).setImageDrawable(getResources().getDrawable(R.drawable.foot));
            text += "Pie ";
        }
        else {
            ((ImageView) findViewById(R.id.selection)).setImageDrawable(getResources().getDrawable(R.drawable.hand));
            text += "Mano ";
        }

        // Derecha o Izquierda
        boolean leftOrRight = new Random().nextBoolean(); // Por defecto, derecha
        if(leftOrRight){
            ((ImageView) findViewById(R.id.selection)).setScaleX(-1f);
            text += "Izquierd";
            if(footOrHand) text += "o";
            else text += "a";
        }
        else {
            text += "Derech";
            if(footOrHand) text += "o";
            else text += "a";
        }

        // Colores
        int color = new Random().nextInt(6) + 1; // Por defecto, derecha
        switch (color) {
            case 1:
                ((ImageView) findViewById(R.id.selection)).setColorFilter(getResources().getColor(R.color.rojo));
                txtColor +="Rojo";
                break;
            case 2:
                ((ImageView) findViewById(R.id.selection)).setColorFilter(getResources().getColor(R.color.amarillo));
                txtColor +="Amarillo";
                break;
            case 3:
                ((ImageView) findViewById(R.id.selection)).setColorFilter(getResources().getColor(R.color.azul));
                txtColor +="Azul";
                break;
            case 4:
                ((ImageView) findViewById(R.id.selection)).setColorFilter(getResources().getColor(R.color.lila));
                txtColor +="Lila";
                break;
            case 5:
                ((ImageView) findViewById(R.id.selection)).setColorFilter(getResources().getColor(R.color.marron));
                txtColor +="Marron";
                break;
            case 6:
                ((ImageView) findViewById(R.id.selection)).setColorFilter(getResources().getColor(R.color.verde));
                txtColor +="Verde";
                break;
            default:
        }

        ((TextView) findViewById(R.id.textSelection)).setText(text);
        ((TextView) findViewById(R.id.textColor)).setText(txtColor);

        int jugador = playersOrder.get(counter);
        if(text.contains("Pie"))  text = text.replace("Pie", "Pié");
        tts.speak(names[jugador], TextToSpeech.QUEUE_ADD, null, null);
        tts.speak(text + txtColor, TextToSpeech.QUEUE_ADD, null, null);
        counter++;
    }

    private int getPlayers(int position) {
        return  (position + 1);
    }

    private int getTime(int position) {
        return ((position + 1) * 5) + 5;
    }

    public void startTimer() {
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, 1 * 1000, time * 1000);
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
          public void run() {
              handler.post(new Runnable() {
                  @Override
                  public void run() {
                      twist();
                  }
              });
          }
        };
    }

    public  void stopTimerTask() {
        if(timer != null) {
            timer.cancel();
            timer = null;
        }
    }
}
