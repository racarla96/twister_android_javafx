package com.example.racarla.twister;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class players_name extends AppCompatActivity {

    private int numPlayers;
    private String[] names = new String[6];

    private EditText[] players = new EditText[6];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players_name);

        Toolbar toolbar = (Toolbar) findViewById(R.id.mainBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        numPlayers = intent.getIntExtra("NUMPLAYERS",-1);
        names = intent.getStringArrayExtra("NAMES");

        players[0] = (EditText) findViewById(R.id.name1);
        players[1] = (EditText) findViewById(R.id.name2);
        players[2] = (EditText) findViewById(R.id.name3);
        players[3] = (EditText) findViewById(R.id.name4);
        players[4] = (EditText) findViewById(R.id.name5);
        players[5] = (EditText) findViewById(R.id.name6);

        for(int i = 0; i < numPlayers; i++) players[i].setEnabled(true);
        for(int i = numPlayers; i < players.length; i++) players[i].setEnabled(false);

        for(int i = 0; i < players.length; i++) players[i].setText(names[i]);

        Button save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((TextView) findViewById(R.id.error)).setText("");
                String textError = "Compruebe el nombre del jugador: ";
                boolean errorName = false;
                for(int i = 0; i < players.length; i++) {
                    if(players[i].getText().toString().trim().length() != 0) names[i] = players[i].getText().toString().trim();
                    else { textError += (i + 1) + " "; errorName = true;}
                }
                if(errorName) ((TextView) findViewById(R.id.error)).setText(textError
                        + "\nSe mantedra el anterior"
                        + "\nEl resto si son guardados");
                else ((TextView) findViewById(R.id.error)).setText("Guardados!");
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        getIntent().putExtra("UPDATENAMES", names);
        setResult(Activity.RESULT_OK, getIntent());
        onBackPressed();
        //finish();
        return true;
    }
}
